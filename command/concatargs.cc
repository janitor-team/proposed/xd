#include "command.ih"

void Command::concatArgs()
{
    auto ranger = Options::instance().args();

    for (char const *arg: ranger)               // all arguments end in /
        (d_arguments += arg) += '/';

    delete[] &*ranger.begin();

    imsg << "Arguments: `" << d_arguments << '\'' << endl;
}
