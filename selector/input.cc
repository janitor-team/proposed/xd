#include "selector.ih"

// static
void Selector::input(string const &text)
{
    Tty tty{ Tty::OFF };

    for (char ch: text)
        ioctl(0, TIOCSTI, &ch);
}
