#include "selector.ih"

void Selector::chdir()
{
    d_returnValue = CD;

    string const &dir{ d_alternatives[d_index] };

    if (d_ioctl)                // --input was specified.
        input("cd " +  dir + '\n');
    else
        cout << dir << '\n';
                                        // Alternatives handles the DIRECT
    d_alternatives.update(d_index);     // update
}
