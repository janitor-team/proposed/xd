#include "selector.ih"

vector<Selector::Block> Selector::blockVector(size_t nBlocks)
{
    vector<Block> blocks(nBlocks, {0, 0, "-+" });

    blocks.front().prompt = "+";
    blocks.front().end = d_blockSize;

    for (size_t idx = 1, end = blocks.size();  idx != end; ++idx)
        set(blocks[idx], blocks[idx - 1].end);

    blocks.back().prompt = "-";

    if (blocks.back().end > d_index)      // back may not exceed 
        blocks.back().end = d_index;      // nAlternatives

    set(*(blocks.rbegin() + 1), blocks.back()); // inspect the last two sizes

    return blocks;
}
