#include "main.ih"

namespace
{
    ArgConfig::LongOption longOpts[] =
    {
        {"add-root",            ArgConfig::Required},
        {"all",                 'a'},
        {"block-size",          'b'},
        {"config-file",         'c'},
        {"directories",         ArgConfig::Required},
        {"generalized-search",  'g'},
        {"help",                'h'},
        {"input",               ArgConfig::None},
        {"history",             ArgConfig::Optional},
        {"history-lifetime",    ArgConfig::Required},
        {"history-maxsize",     ArgConfig::Required},   // history/load.cc
        {"history-position",    ArgConfig::Required},   // top, bottom
        {"history-separate",    ArgConfig::None},
        {"homedir-char",        ArgConfig::Required},
        {"icase",               'i'},
        {"no-input",            ArgConfig::None},
        {"start-at",            ArgConfig::Required},
        {"traditional",         ArgConfig::None},
        {"verbose",             'V'},
        {"version",             'v'},
    };
    auto longEnd = longOpts + size(longOpts);
}

int main(int argc, char **argv)
try
{
    Options::initialize("ab:c:gihvV", longOpts, longEnd, argc, argv,
                        Icmbuild::version, usage);

    Alternatives alternatives;

    alternatives.viable();              // viable alternatives or exception

    alternatives.order();               // history alternatives first or last

                                        // the Selector makes the selection
    Selector selector{ alternatives };  // and inserts the input

    selector.select();                  // make the selection

    return selector.returnValue();      // 0: cd, 1: input, 2: no solution,
                                        // 3: no selection, 4: usage/version 
                                        // 5: error/auto usage
}
catch(...)
{
    return handle(current_exception());
}

