#include "options.ih"

size_t Options::find(char const *longOpt, 
                    string const *begin, string const *end)
{
    string value;
    if (d_arg.option(&value, longOpt) == 0)     // not specified, use the
    {                                           // default
        imsg << "Option or configfile: No key `" << longOpt << '\'' << endl;
        return end - begin;
    }

    auto ret = find_if(
                    begin, end, 
                    [&](string const &entry)
                    {
                        return value == entry;
                    }
                );

    if (ret != end)
        imsg << "Option or config `" << longOpt << ": " << value << '\'' <<
                                                                        endl;
    else
        imsg << "`" << longOpt << " " << value <<
            "' not supported. Using the default" << endl;

    return ret - begin;
}
