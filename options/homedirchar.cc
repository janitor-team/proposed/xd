#include "options.ih"

int Options::homedirChar() const
{
    string value; 
    
    if (d_arg.option(&value, "homedir-char") == 0)
        return '.';

    if ("0123456789/"s.find(value.front()) != string::npos)
        throw Exception{ 1 } << "Character `" << value.front() <<
                                "' cannot be used as homedir-character";

    return value.front();
}
