#include "options.ih"

string Options::readConfigFile()
{
    string confName;
    if (d_arg.option(&confName, 'c'))       // at -c: read the specified 
        d_arg.open(confName);               //        configfile 
    else
    {                                       // otherwise
        confName = d_homeDir + s_defaultConfig;
        
        if (Stat confStat{ confName }; confStat)
        {
            if ((not confStat.mode()) & Stat::UR)
                wmsg << "Can't read " << confName << endl;
            else
                d_arg.open(confName);
        }
    }

    return "Configuration file: " + confName;
}
