//              --history        
//      ---------------------------------
//      no          empty       non-empty
//      default     default     as spec'd   
//      ----------------------------------

#include "options.ih"

void Options::setHistory()              // history filename
{
    if (not d_arg.option(&d_history, "history"))
    {
        imsg << "History file: not used" << endl;
        return;
    }

    if (d_history.empty())
        d_history = s_defaultHistory;       // set default history filename

    imsg << "History file: " << d_history << endl;
}


