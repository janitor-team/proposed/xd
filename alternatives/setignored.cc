#include "alternatives.ih"

void Alternatives::setIgnored()
{
    for                                 // add all ignored dirs in the 
    (                                   // config file
        auto iters = Options::instance().ignore();
        string const &line: ranger(iters.first, iters.second)
    )
        addIgnored(line);               // add line's path to d_ignored
}
