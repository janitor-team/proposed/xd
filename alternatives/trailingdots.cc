#include "alternatives.ih"

// static   returns true if spec has trailing dots
bool Alternatives::trailingDots(string const &spec) 
{
    if  // ignore trailing . and .. directories
    (
        spec.rfind("/.") == spec.length() - 2 
        or
        spec.rfind("/..") == spec.length() - 3
    )
    {
        imsg << "dot-directory" << endl;
        return true;
    }

    return false;
}
