#include "alternatives.ih"

Result Alternatives::traditionalDirs(string const &startDir)
try
{
    if (d_command.empty())                  // no additional dirs specified:
    {
        imsg << "Direct cd to " << startDir << endl;
        return DIRECT;                      // cd to the initial dir
    }

    string dir{ d_initialDir };

    for (auto &element: d_command)
        (dir += element) += "*/";           // add */ to each cmd arg

    dir.pop_back();                         // remove trailing /

    imsg << "Passing `" << dir << "' to glob" << endl;

                                            // find matching elements
    Glob glob(Glob::DIRECTORY, dir, Glob::NOSORT, Glob::DEFAULT);

    for (char const *entry: glob)
        inspect(entry);                     // accept unique dirs.

    return MULTIPLE;
}
catch (exception const &exc)
{
    return MULTIPLE;
}
