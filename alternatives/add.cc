#include "alternatives.ih"

void Alternatives::add(char const *entry)
{
    if (not d_history.find(entry))  // entry is not in the search history
        push_back(entry);           // -> append it to the set of alternatives
    else
    {
        push_front(entry);          // entry is in the history -> show as
        ++d_nInHistory;             // an early alternative.
    }
}
